package com.gcx.pipeline;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

// computes square root of discriminant
class DiscriminantSqrtActor extends DefaultActor {

    @Override
    Behavior<Message> process(Equation data) {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Sqrt start ({})", name, data.getId());
        data.squareRootDiscriminant();
        receiver.tell(data);
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Sqrt end   ({})", name, data.getId());
        return this;
    }




    /**
     * @return instance of this class
     */
    public static Behavior<Message> create() {
        return Behaviors.setup(DiscriminantSqrtActor::new);
    }

    /**
     * Constructor
     */
    private DiscriminantSqrtActor(ActorContext<Message> context) {
        super(context);
    }

    static ActorRef receiver = null;

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    @Override
    Behavior<Message> receiveConsumer(SendConsumer receiver) {
        DiscriminantSqrtActor.receiver = receiver.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }}
