package com.gcx.pipeline;


import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.time.Duration;

class ProducerActor extends AbstractBehavior<Message> {
    static int consumeTime;
    static ActorRef consumer;
    String name = "";

    /**
     * @return instance of this class
     */
    public static Behavior<Message> create(int consumeTime) {
        return Behaviors.setup(context -> new ProducerActor(context, consumeTime));
    }

    /**
     * Constructor
     */
    private ProducerActor(ActorContext<Message> context, int consumeTime) {
        super(context);
        ProducerActor.consumeTime = consumeTime;
        if(Main.PRINT_DEBUG_LOG) context.getLog().info(name + " constructor");
        name = context.getSelf().toString().replaceAll(".*/", "").replaceAll("#.*", "");
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
                .onMessage(Produce.class, this::sendToConsumer)
                .onMessage(SendConsumer.class, this::receiveConsumer)
                .onSignal(PostStop.class, signal -> onPostStop())
                .build();
    }

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    private Behavior<Message> receiveConsumer(SendConsumer c) {
        consumer = c.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info(name + " receiveConsumer");
        return this;
    }

    /**
     * Producing part.
     */
    private Behavior<Message> sendToConsumer(Produce c) {
        getContext().scheduleOnce(Duration.ofMillis(consumeTime), getContext().getSelf(), c);

        Equation e = new Equation();
        consumer.tell(e);
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} send to parser ({})", name, e.getId());
        return this;
    }

    // end on stop signal
    private Behavior<Message> onPostStop() {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info(name + " Producer actor {} stopped", consumeTime);
        return Behaviors.stopped();
    }
}