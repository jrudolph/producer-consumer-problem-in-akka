package com.gcx.pipeline;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

// compute and print solution
class SolutionActor extends DefaultActor {
    @Override
    Behavior<Message> process(Equation data) {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Solution start ({})", name, data.getId());
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Positive Solution ({}), {} -> x1={}", name, data.getId(), data.getEquation(), data.getSolutionPos());
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Negative Solution ({}), {} -> x2={}", name, data.getId(), data.getEquation(), data.getSolutionNeg());
        receiver.tell(data);
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Solution end   ({})", name, data.getId());
        return this;
    }




    /**
     * @return instance of this class
     */
    public static Behavior<Message> create() {
        return Behaviors.setup(SolutionActor::new);
    }

    /**
     * Constructor
     */
    private SolutionActor(ActorContext<Message> context) {
        super(context);
    }

    static ActorRef receiver = null;

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    @Override
    Behavior<Message> receiveConsumer(SendConsumer receiver) {
        SolutionActor.receiver = receiver.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }
}
