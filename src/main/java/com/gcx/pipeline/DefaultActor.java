package com.gcx.pipeline;

import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

// general class for each actor
public class DefaultActor extends AbstractBehavior<Message>  {
    String name = "";

    /**
     * Constructor
     */
    public DefaultActor(ActorContext<Message> context) {
        super(context);
        if(Main.PRINT_DEBUG_LOG) context.getLog().info("{} constructor", name);
        name = context.getSelf().toString().replaceAll(".*/", "").replaceAll("#.*", "");
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
            .onMessage(Produce.class, this::sendToConsumer)
            .onMessage(Equation.class, this::process)
            .onMessage(SendConsumer.class, this::receiveConsumer)
            .onMessage(LogStats.class, this::onLog)
            .onSignal(PostStop.class, signal -> onPostStop())
            .build();
    }

    // dummy methods
    Behavior<Message> sendToConsumer(Produce c) { return this;}
    Behavior<Message> receiveConsumer(SendConsumer c) {return this;}
    Behavior<Message> process(Equation data) {return this;}
    Behavior<Message> onLog(LogStats a) {return this;}

    // end on stop signal
    Behavior<Message> onPostStop() {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("Actor {} stopped", name);
        return Behaviors.stopped();
    }
}
