package com.gcx.pipeline;

import akka.actor.typed.ActorSystem;

public class Main {

    public final static boolean PRINT_DEBUG_LOG = false;

    public static void main(String[] args) {
        // application for dummy evaluation of quadratic equation
        // In one actor (DiscriminantComputationActor) is time consuming operation for simulation of CPU load

        // creates main actor
        ActorSystem<Message> mainActor = ActorSystem.apply(MainActor.create(), "main_actor");
        mainActor.tell(new Setup(1000));

        // start producing
        mainActor.tell(new Produce());
        // start performance
        mainActor.tell(new LogStats());

    }
}
