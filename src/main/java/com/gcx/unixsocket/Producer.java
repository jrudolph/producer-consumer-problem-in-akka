package com.gcx.unixsocket;

import org.newsclub.net.unix.AFUNIXSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

import javax.annotation.Nonnull;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketAddress;

public class Producer {
    static public AFUNIXSocket socketConnection;
    static private SocketAddress socketAddress;
    static DataOutputStream osw;

    /**
     * Start producing data
     */
    public static void main(String[] args) {
        int count = 100;

        Data data = new Data("test");
        if (connect()){
            for(int i = 0; i < count; i++) {
                data.setValue(i);
                sendToSocket(data);
            }
            cleanUp();
            return;
        }
        System.err.println("problem with sending data");
    }


    /**
     * Connect UNIX domain socket
     * @return success
     */
    static public boolean connect() {
        try {
            if (socketConnection == null || socketConnection.isClosed() || !socketConnection.isConnected()) {
                socketConnection = AFUNIXSocket.newInstance();
                socketAddress = new AFUNIXSocketAddress(new File(Consumer.SOCKET_FILE));
                socketConnection.connect(socketAddress);
                osw = new DataOutputStream(socketConnection.getOutputStream());
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Clean up socket
     */
    static private void cleanUp() {
        try {
            socketConnection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send data via socket
     * @param data object for transferring
     * @return
     */
    static private boolean sendToSocket(@Nonnull Data data) {
        System.out.println(String.format("Reporting event %s - %d", data.getName(), data.getValue()));
        try {
            osw.write(Data.serialize(data));
            osw.flush();
            System.out.println("Data are written");
            return true;
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
