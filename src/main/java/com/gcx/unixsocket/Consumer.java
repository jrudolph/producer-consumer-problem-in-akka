package com.gcx.unixsocket;

import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.SupervisorStrategy;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import akka.stream.Materializer;
import akka.stream.alpakka.unixdomainsocket.javadsl.UnixDomainSocket;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.util.ByteString;

import java.io.File;
import java.util.concurrent.CompletionStage;

public class Consumer extends AbstractBehavior<Data> {

    static final public String SOCKET_FILE = "/tmp/nba.sock";

    /**
     * Start consumer - run server and receive data
     */
    public static void main(String[] args) {
        ActorSystem<Data> actor = ActorSystem.apply(Consumer.create(), "actor");
    }


    private Consumer(ActorContext<Data> context) {
        super(context);
        setUpSocket();
    }

    public static Behavior<Data> create() {
        return Behaviors.supervise(Behaviors.setup(Consumer::new)).onFailure(Exception.class, SupervisorStrategy.restart());
    }

    @Override
    public Receive<Data> createReceive() {
        return newReceiveBuilder()
            .build();
    }


    /**
     * Counter of received messages
     */
    private static long counter;

    /**
     * set up listener for unix domain socket
     */
    private void setUpSocket() {

        akka.actor.ActorSystem system = akka.actor.ActorSystem.create();
        UnixDomainSocket uds = UnixDomainSocket.get(system);
        Materializer materializer = Materializer.createMaterializer(getContext().getSystem());

        final Source<UnixDomainSocket.IncomingConnection, CompletionStage<UnixDomainSocket.ServerBinding>> connections = uds.bind(new File(SOCKET_FILE));

        // define listener
        connections.runForeach(
                connection -> {
                    counter = 0;
                    System.out.println("New connection from: " + connection.localAddress().toString());

                    final Flow<ByteString, ByteString, NotUsed> socketPipeline = Flow.of(ByteString.class)
                            .map(bytes -> {      // processing message - deserialize and send it to next actor
                                Data data = Data.deserialize(bytes.toArray());
                                System.out.println(String.format("received #%2d; real #%2d", data.getValue(), counter++));
                                return bytes;
                            })
                            .map(s -> ByteString.fromString(""))                // return empty message, do not close connection
//                        .map(null)                                          // close connection
                    ;
                    connection.handleWith(socketPipeline, materializer);
                },
                system
        );
    }
}
