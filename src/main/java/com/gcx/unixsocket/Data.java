package com.gcx.unixsocket;

import java.io.*;

/**
 * POJO
 */
public class Data implements Serializable {
    private static final long serialVersionUID = -960243937582271319L;
    private String name;
    private int value;

    public Data(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    /**
     *
     * @param data object
     * @return serialized object
     */
    public static byte[] serialize(Data data) {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        try {
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(data);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return bo.toByteArray();
    }

    /**
     * @param serializedData bytes
     * @return serialized Data object
     */
    public static Data deserialize(byte[] serializedData) {
        ByteArrayInputStream bi = new ByteArrayInputStream(serializedData);
        try {
            ObjectInputStream si = new ObjectInputStream(bi);
            return (Data) si.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
