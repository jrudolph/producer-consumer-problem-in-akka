package com.gcx.basic;

import akka.actor.typed.*;
import akka.actor.typed.javadsl.*;
import akka.actor.typed.receptionist.Receptionist;
import akka.actor.typed.receptionist.ServiceKey;

import java.time.Duration;
import java.util.stream.IntStream;


class MainActor extends AbstractBehavior<Message>  {

    // references to actors
    private ActorRef<Message> producer = null;
    private ActorRef<Message> consumer = null;

    //dispatcher configuration from application.conf file
    private DispatcherSelector configDispatcher = DispatcherSelector.fromConfig("actor-dispatcher");
    private DispatcherSelector producerDispatcher = DispatcherSelector.fromConfig("producer-dispatcher");

    final int maxConsumerActors = 30;

    /**
     * Constructor of actor manager
     * @param context akka context
     */
    private MainActor(ActorContext<Message> context) {
        super(context);
        // stop program after 15 minutes
        getContext().scheduleOnce(Duration.ofMinutes(15), getContext().getSelf(), new EndUp());
    }

    /**
     * @return instance of this class
     */
    static Behavior<Message> create() {
        return Behaviors.setup(MainActor::new);
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
                .onMessage(Setup.class, this::onSetup)
                .onMessage(EndUp.class, this::onEnd)
                .onMessage(Produce.class, this::onStartProducing)
                .onSignal(PostStop.class, signal -> onPostStop())
                .build();
    }

    /**
     * @param produce message which cause start of production
     */
    private Behavior<Message> onStartProducing(Produce produce) {
        // if all actors are setup
        if (consumer != null && produce != null) {
            produce.setConsumer(consumer);
            producer.tell(produce);
        } else {
            // if not plan production start on next minute
            getContext().scheduleOnce(Duration.ofMinutes(1), getContext().getSelf(), produce);
        }
        return this;
    }

    /**
     * @param setup message for setup connection between actors
     */
    private Behavior<Message> onSetup(Setup setup) {
        consumer = createRouteGroupConsumer(setup.getConsumeTime());
        getContext().getLog().info("Create consumer {}", consumer);

        producer = getContext().spawn(ProducerActor.create(setup.getSendInterval()), "producer", producerDispatcher);
        getContext().getLog().info("Create producer {}", producer);

        return this;
    }

    /**
     * Creates group of consumer actors with random messaging.
     * Number of actors are fixed size.
     *
     * @param time of consuming data
     * @return reference to group of actors
     */
    private ActorRef<Message> createRoutePoolConsumerRandom(int time) {
        int poolSize = 4;
        PoolRouter<Message> pool = Routers.pool(poolSize, Behaviors.supervise(ConsumerActor.create(time)).onFailure(SupervisorStrategy.restart()));
        return getContext().spawn(pool, "consumer-pool");
    }

    /**
     * Creates group of consumer actors with round-robin messaging.
     * Number of actors are fixed size.
     *
     * @param time of consuming data
     * @return reference to group of actors
     */
    private ActorRef<Message> createRoutePoolConsumerRR(int time) {
        int poolSize = 4;
        PoolRouter<Message> pool = Routers.pool(poolSize, Behaviors.supervise(ConsumerActor.create(time)).onFailure(SupervisorStrategy.restart())).withRoundRobinRouting();
        return getContext().spawn(pool, "consumer-pool");
    }

    /**
     * Creates group of consumer actors with round-robin messaging.
     * Number of actors are dynamically de/increasing according consuming load.
     *
     * @param time of consuming data
     * @return reference to group of actors
     */
    private ActorRef<Message> createRouteGroupConsumer(int time) {
        // creates service key - identification which connect all actor with same key into one group
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-group-consumer");

        // creating actors
        IntStream.range(1, maxConsumerActors)
                .forEach(i -> getContext()
                        .getSystem()
                        .receptionist()
                        .tell(Receptionist.register(serviceKey, getContext().spawn(ConsumerActor.create(time), "consumer"+i, configDispatcher))));

        // config group
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();

        // create group
        return getContext().spawn(group, "consumer-group");
    }

    /**
     * @param end message for stopping this actor
     */
    private Behavior<Message> onEnd(EndUp end) {
        return Behaviors.stopped();
    }

    /**
     * Signal for stopping this actor
     */
    private Behavior<Message> onPostStop() {
        getContext().getLog().info("Actor manager ends");
        return Behaviors.stopped();
    }
}
