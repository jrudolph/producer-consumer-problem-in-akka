package com.gcx.basic;


import akka.actor.typed.ActorSystem;

public class Main {

    public static void main(String[] args) {
        // creates main actor
        ActorSystem<Message> mainActor = ActorSystem.apply(MainActor.create(), "actorManager");

        mainActor.tell(new Setup(100, 390));
        mainActor.tell(new Produce(null));
    }
}
